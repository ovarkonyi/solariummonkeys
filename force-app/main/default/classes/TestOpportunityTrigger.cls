@IsTest
private class TestOpportunityTrigger {
    
    

    // INSERT QUOTE, QUOTELINEITEM

    @IsTest
    static void testOppStageWhenInsertingQuoteLineItemWithoutDiscount() {
        Account acc = createAcc();
        Opportunity opp = createOpp(acc.Id, 'Qualifying');
        Quote q = createQuote(opp.Id);
        Product2 p = createProduct2();
		QuoteLineItem qli = createQuoteLineItem(q.Id, p.Id, 1, 0);
        
        system.assertEquals('Proposal/Price Quote', getOpportunity(opp.Id).StageName);
    }
    
    @IsTest
    static void testOppStageWhenInsertingQuoteLineItemWithDiscount() {
        Account acc = createAcc();
        Opportunity opp = createOpp(acc.Id, 'Qualifying');
        Quote q = createQuote(opp.Id);
        Product2 p = createProduct2();
		QuoteLineItem qli = createQuoteLineItem(q.Id, p.Id, 1, 20);
        
        system.assertEquals('WaitingForApproval', getOpportunity(opp.Id).StageName);
    }
    
    @IsTest
    static void testQuoteStatusWhenInsertingQuoteLineItemWithoutDiscount() {
        Account acc = createAcc();
        Opportunity opp = createOpp(acc.Id, 'Qualifying');
        Quote q = createQuote(opp.Id);
        Product2 p = createProduct2();
		QuoteLineItem qli = createQuoteLineItem(q.Id, p.Id, 1, 0);
        
        system.assertEquals('Approved', getQuote(q.Id).Status);
    }
    
    @IsTest
    static void testQuoteStatusWhenInsertingQuoteLineItemWithDiscount() {
        Account acc = createAcc();
        Opportunity opp = createOpp(acc.Id, 'Qualifying');
        Quote q = createQuote(opp.Id);
        Product2 p = createProduct2();
		QuoteLineItem qli = createQuoteLineItem(q.Id, p.Id, 1, 20);
        
        system.assertEquals('In Review', getQuote(q.Id).Status);
    }
    
    @IsTest
    static void testOppStageWhenInsertingQuote() {
        Account acc = createAcc();
        Opportunity opp = createOpp(acc.Id, 'Qualifying');
        Quote q = createQuote(opp.Id);
        
        system.assertEquals('Qualifying', getOpportunity(opp.Id).StageName);
    }
    
	    //INSERT MORE THAN ONE QUOTELINEITEM
    
    @IsTest
    static void testOppStageWhenInsertingTwoQuoteLineItemWithoutDiscountAndWithDiscount() {
        Account acc = createAcc();
        Opportunity opp = createOpp(acc.Id, 'Qualifying');
        Quote q = createQuote(opp.Id);
        Product2 p = createProduct2();
		QuoteLineItem qli = createQuoteLineItem(q.Id, p.Id, 1, 0);
        QuoteLineItem qli2 = createQuoteLineItem(q.Id, p.Id, 1, 20);
        
        system.assertEquals('WaitingForApproval', getOpportunity(opp.Id).StageName);
    }
    
    @IsTest
    static void testOppStageWhenInsertingTwoQuoteLineItemWithDiscountAndWithoutDiscount() {
        Account acc = createAcc();
        Opportunity opp = createOpp(acc.Id, 'Qualifying');
        Quote q = createQuote(opp.Id);
        Product2 p = createProduct2();
		QuoteLineItem qli = createQuoteLineItem(q.Id, p.Id, 1, 20);
        QuoteLineItem qli2 = createQuoteLineItem(q.Id, p.Id, 1, 0);
        
        system.assertEquals('WaitingForApproval', getOpportunity(opp.Id).StageName);
    }
    
    @IsTest
    static void testQuoteStatusWhenInsertingTwoQuoteLineItemWithoutDiscountAndWithDiscount() {
        Account acc = createAcc();
        Opportunity opp = createOpp(acc.Id, 'Qualifying');
        Quote q = createQuote(opp.Id);
        Product2 p = createProduct2();
		QuoteLineItem qli = createQuoteLineItem(q.Id, p.Id, 1, 20);
        QuoteLineItem qli2 = createQuoteLineItem(q.Id, p.Id, 1, 0);
        
        system.assertEquals('In Review', getQuote(q.Id).Status);
    }
    
    @IsTest
    static void testQuoteStatusWhenInsertingTwoQuoteLineItemWithDiscountAndWithoutDiscount() {
        Account acc = createAcc();
        Opportunity opp = createOpp(acc.Id, 'Qualifying');
        Quote q = createQuote(opp.Id);
        Product2 p = createProduct2();
		QuoteLineItem qli = createQuoteLineItem(q.Id, p.Id, 1, 0);
        QuoteLineItem qli2 = createQuoteLineItem(q.Id, p.Id, 1, 20);
        
        system.assertEquals('In Review', getQuote(q.Id).Status);
    }

    // UPDATE QUOTE, QUOTELINEITEM

    @IsTest
    static void testOppStageWhenUpdateQuoteLineItemWithoutDiscountToWithDiscount() {
        Account acc = createAcc();
        Opportunity opp = createOpp(acc.Id, 'Qualifying');
        Quote q = createQuote(opp.Id);
        Product2 p = createProduct2();
		QuoteLineItem qli = createQuoteLineItem(q.Id, p.Id, 1, 0);
        qli.Discount = 20;
        update qli;

        system.assertEquals('WaitingForApproval', getOpportunity(opp.Id).StageName);
    }
    
    @IsTest
    static void testOppStageWhenUpdateQuoteLineItemWithDiscountToWithoutDiscount() {
        Account acc = createAcc();
        Opportunity opp = createOpp(acc.Id, 'Qualifying');
        Quote q = createQuote(opp.Id);
        Product2 p = createProduct2();
		QuoteLineItem qli = createQuoteLineItem(q.Id, p.Id, 1, 20);
        qli.Discount = 0;
        update qli;

        system.assertEquals('Proposal/Price Quote', getOpportunity(opp.Id).StageName);
    }
    
    @IsTest
    static void testQuoteStatusWhenUpdateQuoteLineItemWithoutDiscountToWithDiscount() {
        Account acc = createAcc();
        Opportunity opp = createOpp(acc.Id, 'Qualifying');
        Quote q = createQuote(opp.Id);
        Product2 p = createProduct2();
		QuoteLineItem qli = createQuoteLineItem(q.Id, p.Id, 1, 0);
        qli.Discount = 20;
        update qli;
        
        system.assertEquals('In Review', getQuote(q.Id).Status);
    }
    
    @IsTest
    static void testQuoteStatusWhenUpdateQuoteLineItemWithDiscountToWithoutDiscount() {
        Account acc = createAcc();
        Opportunity opp = createOpp(acc.Id, 'Qualifying');
        Quote q = createQuote(opp.Id);
        Product2 p = createProduct2();
		QuoteLineItem qli = createQuoteLineItem(q.Id, p.Id, 1, 20);
        qli.Discount = 0;
        update qli;
        
        system.assertEquals('Approved', getQuote(q.Id).Status);
    }


    
    // INSERT QUOTELINEITEM CHECK TASK
    
    @IsTest
    static void testTaskDescriptionWhenInsertingQuoteLineItemWithDiscount() {
        Account acc = createAcc();
        Opportunity opp = createOpp(acc.Id, 'Qualifying');
        Quote q = createQuote(opp.Id);
        Product2 p = createProduct2();
		QuoteLineItem qli = createQuoteLineItem(q.Id, p.Id, 1, 20);
        
        system.assertEquals('Please approve this QuoteLineItem!', getTask(q.Id).Description);
    }
    
    @IsTest
    static void testTaskAccountWhenInsertingQuoteLineItemWithDiscount() {
        Account acc = createAcc();
        Opportunity opp = createOpp(acc.Id, 'Qualifying');
        Quote q = createQuote(opp.Id);
        Product2 p = createProduct2();
		QuoteLineItem qli = createQuoteLineItem(q.Id, p.Id, 1, 20);
        
        system.assertEquals(acc.Id, getTask(q.Id).WhatId);
    }
    
    @IsTest
    static void testTaskOwnerWhenInsertingQuoteLineItemWithDiscount() {
        Account acc = createAcc();
        Opportunity opp = createOpp(acc.Id, 'Qualifying');
        Quote q = createQuote(opp.Id);
        Product2 p = createProduct2();
		QuoteLineItem qli = createQuoteLineItem(q.Id, p.Id, 1, 20);
        
        system.assertEquals(getUser().ManagerId, getTask(q.Id).OwnerId);
    }
    
    // UPDATE QUOTELINEITEM CHECK TASK
    
    @IsTest
    static void testTaskDescriptionWhenUpdateQuoteLineItemWithoutDiscountToWithDiscount() {
        Account acc = createAcc();
        Opportunity opp = createOpp(acc.Id, 'Qualifying');
        Quote q = createQuote(opp.Id);
        Product2 p = createProduct2();
		QuoteLineItem qli = createQuoteLineItem(q.Id, p.Id, 1, 0);
        qli.Discount = 20;
        update qli;
        
        system.assertEquals('Please approve this QuoteLineItem!', getTask(q.Id).Description);
    }
    
    // UPDATE OPPORTUNITY CHECK TASK
    
    @IsTest
    static void testTaskDescriptionWhenUpdateOpportunityToPaid() {
        Account acc = createAcc();
        Opportunity opp = createOpp(acc.Id, 'Qualifying');
        Quote q = createQuote(opp.Id);
        Product2 p = createProduct2();
		QuoteLineItem qli = createQuoteLineItem(q.Id, p.Id, 1, 0);
        opp.Stagename = 'Order';
        update opp;
        opp.Stagename = 'Paid';
        update opp;
        
        system.assertEquals('Please deliver this asset to client!', getTask(q.Id).Description);
    }
    
    @IsTest
    static void testTaskOwnerIdWhenUpdateOpportunityToPaid() {
        Account acc = createAcc();
        Opportunity opp = createOpp(acc.Id, 'Qualifying');
        Quote q = createQuote(opp.Id);
        Product2 p = createProduct2();
		QuoteLineItem qli = createQuoteLineItem(q.Id, p.Id, 1, 0);
        opp.Stagename = 'Order';
        update opp;
        opp.Stagename = 'Paid';
        update opp;
        
        system.assertEquals(getDeploymentmanager().Id, getTask(q.Id).OwnerId);
    }
	
    // UPDATE OPPORTUNITY INSERT ASSET
    
    // DELETE QUOTELINEITEM

    @IsTest
    static void testQuoteStatusWhenDeleteQuoteLineItemAndNoMoreQLI() {
        Account acc = createAcc();
        Opportunity opp = createOpp(acc.Id, 'Qualifying');
        Quote q = createQuote(opp.Id);
        Product2 p = createProduct2();
		QuoteLineItem qli = createQuoteLineItem(q.Id, p.Id, 1, 0);
        delete qli;

        system.assertEquals('Draft', getQuote(q.Id).Status);
    }
    
    @IsTest
    static void testQuoteStatusWhenDeleteQuoteLineItemAndThereIsMoreQLIWithoutDiscount() {
        Account acc = createAcc();
        Opportunity opp = createOpp(acc.Id, 'Qualifying');
        Quote q = createQuote(opp.Id);
        Product2 p = createProduct2();
		QuoteLineItem qli = createQuoteLineItem(q.Id, p.Id, 1, 0);
        QuoteLineItem qli2 = createQuoteLineItem(q.Id, p.Id, 1, 0);
        delete qli2;

        system.assertEquals('Approved', getQuote(q.Id).Status);
    }
    
    @IsTest
    static void testQuoteStatusWhenDeleteQuoteLineItemAndThereIsMoreQLIWithDiscount() {
        Account acc = createAcc();
        Opportunity opp = createOpp(acc.Id, 'Qualifying');
        Quote q = createQuote(opp.Id);
        Product2 p = createProduct2();
		QuoteLineItem qli = createQuoteLineItem(q.Id, p.Id, 1, 20);
        QuoteLineItem qli2 = createQuoteLineItem(q.Id, p.Id, 1, 0);
        delete qli2;

        system.assertEquals('In Review', getQuote(q.Id).Status);
    }
    


    // VALIDATIONS

    @IsTest
    static void testValidateInsertingOppWaitingForApprove() {
        String msg;
        try {
            Account acc = createAcc();
            Opportunity opp = createOpp(acc.Id, 'WaitingForApprove');
        } catch(Exception ex){
            msg = getErrorMsg(ex);
        }
        system.assertEquals('Opportunities can be created only in Qualifying status.', msg);
    }

    @IsTest
    static void testValidateInsertingOppProposalPriceQuote() {
        String msg;
        try {
            Account acc = createAcc();
            Opportunity opp = createOpp(acc.Id, 'Proposal/Price Quote');
        } catch(Exception ex){
            msg = getErrorMsg(ex);
        }
        system.assertEquals('Opportunities can be created only in Qualifying status.', msg);
    }

    @IsTest
    static void testValidateInsertingOppOrder() {
        String msg;
        try {
            Account acc = createAcc();
            Opportunity opp = createOpp(acc.Id, 'Order');
        } catch(Exception ex){
            msg = getErrorMsg(ex);
        }
        system.assertEquals('Opportunities can be created only in Qualifying status.', msg);
    }
    
    @IsTest
    static void testValidateInsertingOppPaid() {
        String msg;
        try {
            Account acc = createAcc();
            Opportunity opp = createOpp(acc.Id, 'Paid');
        } catch(Exception ex){
            msg = getErrorMsg(ex);
        }
        system.assertEquals('Opportunities can be created only in Qualifying status.', msg);
    }

    @IsTest
    static void testValidateUpdateOppPaidToOrder() {
        String msg;
        try {
            Account acc = createAcc();
            Opportunity opp = createOpp(acc.Id, 'Qualifying');
            opp.Stagename = 'Proposal/Price Quote';
            update opp;
            opp.Stagename = 'Order';
            update opp;
            opp.Stagename = 'Paid';
            update opp;
            opp.Stagename = 'Order';
            update opp;
        } catch(Exception ex){
            msg = getErrorMsg(ex);
        }
        system.assertEquals('Opportunities cannot degrade from Paid.', msg);
    }
    
    @IsTest
    static void testValidateUpdateOppOrderToWaitingForApprove() {
        String msg;
        try {
            Account acc = createAcc();
            Opportunity opp = createOpp(acc.Id, 'Qualifying');
            opp.Stagename = 'Proposal/Price Quote';
            update opp;
            opp.Stagename = 'Order';
            update opp;
            opp.Stagename = 'WaitingForApprove';
            update opp;
        } catch(Exception ex){
            msg = getErrorMsg(ex);
        }

        system.assertEquals('Opportunities cannot degrade from Paid.', msg);
    }
    
    @IsTest
    static void testValidateUpdateOppOrderToProposalPriceQuote() {
        String msg;
        try {
            Account acc = createAcc();
            Opportunity opp = createOpp(acc.Id, 'Qualifying');
            opp.Stagename = 'Proposal/Price Quote';
            update opp;
            opp.Stagename = 'Order';
            update opp;
            opp.Stagename = 'Proposal/Price Quote';
            update opp;
        } catch(Exception ex){
            msg = getErrorMsg(ex);
        }

        system.assertEquals('Opportunities cannot degrade from Paid.', msg);
    }

    @IsTest
    static void testValidateUpdateOppClosedWonToNothing() {
        String msg;
        try {
            Account acc = createAcc();
            Opportunity opp = createOpp(acc.Id, 'Qualifying');
            opp.Stagename = 'Proposal/Price Quote';
            update opp;
            opp.Stagename = 'Order';
            update opp;
            opp.Stagename = 'Paid';
            update opp;
            opp.Stagename = 'Delivered';
            update opp;
            opp.Stagename = 'Closed Won';
            update opp;
            opp.Stagename = 'Delivered';
            update opp;
        } catch(Exception ex){
            msg = getErrorMsg(ex);
        }

        system.assertEquals('The stage of an Opportunity can not be changed from Closed Won.', msg);
    }
    
    @IsTest
    static void testValidateUpdateOppClosedLostToNothing() {
        String msg;
        try {
            Account acc = createAcc();
            Opportunity opp = createOpp(acc.Id, 'Qualifying');
            opp.Stagename = 'Proposal/Price Quote';
            update opp;
            opp.Stagename = 'Order';
            update opp;
            opp.Stagename = 'Paid';
            update opp;
            opp.Stagename = 'Delivered';
            update opp;
            opp.Stagename = 'Closed Lost';
            update opp;
            opp.Stagename = 'Delivered';
            update opp;
        } catch(Exception ex){
            msg = getErrorMsg(ex);
        }
        system.assertEquals('The stage of an Opportunity can not be changed from Closed Lost.', msg);
    }
	
    /*
    @IsTest
    static void testScheduledSetOppToLost() {

        Account acc = createAcc();
        List<Opportunity> oppsToInsert = new List<Opportunity>();

        for(integer i = 0; i < 150; i++) {
            Opportunity opp = new Opportunity(
                    Name = 'Test Opp',
                    AccountId = acc.Id,
                    StageName = 'Qulifying',
                    CloseDate = System.today().addDays(-1)
            );
            oppsToInsert.add(opp);
        }
        insert oppsToInsert;

        test.startTest();
        //ScheduledSetOpportunityToLost sc = new ScheduledSetOpportunityToLost();
        //sc.execute(null);
        test.stopTest();

        //system.assertEquals(150, [SELECT COUNT() FROM Opportunity WHERE Status__c = 'Lost']);
	}
	*/
    
    private static Account createAcc(){
        Account acc = new Account(Name = 'Test Acc');
        insert acc;
        return acc;
    }

    private static Opportunity createOpp(Id accId, String stagename){
        Opportunity opp = new Opportunity(
            Name = 'Test Opp',
            AccountId = accId,
            StageName = stagename,
            CloseDate = System.today().addYears(1)
        );
        insert opp;
        return opp;
    }
    private static Quote createQuote(Id oppId){
        Quote q = new Quote(
            Name = 'Test Quote',
            OpportunityId = oppId,
            Status = 'Draft',
            Created_Date__c = System.today(),
            ExpirationDate = System.today().addMonths(1)
        );
        insert q;
        return q;
    }
    private static Product2 createProduct2(){
        Product2 p = new Product2(
            Name = 'Test Product',
            isActive = true
        );
        insert p;
        return p;
    }
    private static QuoteLineItem createQuoteLineItem(Id quoteId, Id product2Id, Integer quantity, Integer discount){
        QuoteLineItem qli = new QuoteLineItem(
            QuoteId = quoteId,
            Quantity = quantity,
            Discount = discount,
            Product2Id = product2Id//01t5J000000nnlaQAA
        );
        insert qli;
        return qli;
    }
    private static Task createTask(Id userId, Id contId, Id qId, Id assId){
        Task t = new Task(
            OwnerId = userId,
            Status = 'Not Started',
            Subject = 'Other',
            Priority = 'Normal',
            ActivityDate = System.today(),
            WhoId = contId,
            Quote_For_Task__c = qId,
            Asset_For_Task__c = assId,
            Description = 'Please approve this QuoteLineItem!'
        );
        insert t;
        return t;        
    }
    
    private static Opportunity getOpportunity(Id oppId){
        return [SELECT StageName FROM Opportunity WHERE Id = :oppId LIMIT 1];
    }
    private static Quote getQuote(Id qId){
        return [SELECT Status FROM Quote WHERE Id = :qId LIMIT 1];
    }
    private static Task getTask(Id quoteId){
        return [SELECT OwnerId, Description, Quote_For_Task__c, WhatId FROM Task WHERE Quote_For_Task__c = :quoteId LIMIT 1];
    }
    private static User getUser() {
    	User currentUser = [SELECT Id, Name FROM User WHERE Id =: UserInfo.getUserId()];
        return currentUser;
    }
    private static User getDeploymentmanager() {
    	User currentUser = [SELECT Id, Name, UserRole.Name FROM User WHERE UserRole.Name = 'Deployment Manager' LIMIT 1];
        return currentUser;
    }

    private static String getErrorMsg(Exception ex){
        return ex.getMessage().substringBetween('FIELD_CUSTOM_VALIDATION_EXCEPTION, ', ': [StageName]');
    }

}
