global class ScheduledUnPaidOpportunity implements Schedulable{
	global void execute(SchedulableContext ex){
        Date payDeadline = System.today();
        List<Opportunity> notPaidOpportunities = [
            								SELECT Id, CloseDate, OwnerId 
            								FROM Opportunity 
            								WHERE StageName = 'Order' AND CloseDate < :payDeadline 
            								LIMIT 100
        									];
        Set<Id> ids = (new Map<Id, Opportunity>([SELECT Id FROM Opportunity])).keySet();
		List<Quote> quotes = [SELECT Id,
                                 Name, 
                                 Accountid,
                                 OpportunityId,
                                 Opportunity.Name, 
                                 Opportunity.StageName,
                                 (SELECT Id FROM QuoteLineItems) 
                             FROM Quote 
                             WHERE OpportunityId IN :ids];
        for(Quote q: quotes){
            for(Opportunity o: notPaidOpportunities) {
                if(q.OpportunityId == o.Id){
                    String userId = UserInfo.getUserId();
                    Task t = new Task();
                    t.OwnerId = userId;
                    t.Subject = 'Bill is not paid, contact the costumer';
                    t.Status = 'Open';
                    t.Priority = 'High';
                    t.ActivityDate = System.today() + 1;
                    t.WhatId = o.Id;
                    t.Quote_For_Task__c = q.Id;
                    insert t;
                 }
            }
        }
	}
}
