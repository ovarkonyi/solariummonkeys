/*
Ha az adott Quote-hoz egy olyan QuoteLineItem is tartozik, amelynek a Discount mezője nem null, 
azaz diszkont ár van megadva, akkor a Quote státuszát In Review-re állitjuk.

QuoteLineItem beszúrás után:
- Kikeressük azokat a Quote-eket, amelyeknek közük van a beszúrt QuoteLineItem-okhoz.
- Megvizsgáljuk, hogy a QuoteLineItem Discount mezője ki van-e töltve, a Quote Status mezőjét ennek megfelelően állitjuk:
	Ha Discount-ot adunk meg, akkor a Quote Status mezőjét In Review-re állitjuk.
	Ha nem adunk meg a Discount-ot, akkor egy metódussal megnézzük, hogy van-e még Discount-os QuoteLineItem.

QuoteLineItem frissités után:
- Kikeressük azokat a Quote-eket, amelyeknek közük van a beszúrt QuoteLineItem-okhoz, változott a Discount mezőjük.
- Összehasonlitjuk a QuoteLineItem régi Discount mezőjét az új Discount mezővel, a Quote Status mezőjét ennek megfelelően állitjuk:
	Ha Discount-ot adunk meg, akkor a Quote Status mezőjét In Review-re állitjuk.
	Ha töröljük a Discount mezőt, akkor egy metódussal megnézzük, hogy van-e még Discount-os QuoteLineItem.

QuoteLineItem törlés után:
- Kikeressük azokat a Quote-eket, amelyeknek közük van a törölt QuoteLineItem-ekhez.
- Megvizsgáljuk, hogy az adott Quote QuteLineItem lista méretét:
    Ha nulla, azaz nincs több QuoteLineItem a Quote-on, akkor Quote Status mezőjét Draft-ra állitjuk.
	Ha nem nulla, akkor egy metódussal megnézzük, hogy van-e még Discount-os QuoteLineItem. 
*/

public class QuoteLineItemTriggerHandler {
    
    public static final String DRAFT = 'Draft';
    public static final String INREVIEW = 'In Review';
    public static final String APPROVED = 'Approved';
    

    
    public static void handleBeforeInsert(List<QuoteLineItem> triggerNew){

    }
    
    
    public static void handleBeforeUpdate(Map<Id, QuoteLineItem> triggerOldMap, List<QuoteLineItem> triggerNew){
		
    }
    
    
    public static void handleAfterInsert(List<QuoteLineItem> triggerNew){
        
        //Kikeressük azokat a Quote-eket, amelyeknek közük van a beszúrt QuoteLineItem-okhoz és a quoteMapIds-ba tesszük.
        Set<Id> quoteIds = new Set<Id>();
        for(QuoteLineItem qli : triggerNew){
            if(qli.QuoteId != null){
                quoteIds.add(qli.QuoteId);
            }            
        }        
        Map<Id,Quote> quoteMapIds = new Map<Id,Quote>([SELECT Id, Status, (SELECT Id, Discount FROM QuoteLineItems) FROM Quote WHERE Id IN :quoteIds]); 
        
        //Megvizsgáljuk, hogy a QuoteLineItem Discount mezője ki van-e töltve, a Quote Status mezőjét ennek megfelelően állitjuk.
        for(QuoteLineItem qli : triggerNew){
            if(qli.QuoteId != null){
                if(quoteMapIds.get(qli.QuoteId).Status !='Accepted' || quoteMapIds.get(qli.QuoteId).Status !='Denied' || quoteMapIds.get(qli.QuoteId).Status !='Rejected' || quoteMapIds.get(qli.QuoteId).Status !='Presented'){
                    if(qli.Discount != null){//Ha Discount-ot adunk meg, akkor a Quote Status mezőjét In Review-re állitjuk.
                        quoteMapIds.get(qli.QuoteId).Status = INREVIEW;
                    }else if(qli.Discount == null){//Ha nem adunk meg a Discount-ot, akkor egy metódussal megnézzük, hogy van-e még Discount-os QuoteLineItem.
                        List<QuoteLineItem> qliList =  quoteMapIds.get(qli.QuoteId).QuoteLineItems;
                        quoteMapIds.get(qli.QuoteId).Status = quoteHasInReviewQuoteLineItem(qliList);
                    }
                }
            }            
        }
        update quoteMapIds.values();        
    }
    
    
    public static void handleAfterUpdate(Map<Id, QuoteLineItem> triggerOldMap, List<QuoteLineItem> triggerNew){
        //Kikeressük azokat a Quote-eket, amelyeknek közük van a beszúrt QuoteLineItem-okhoz, változott a Discount mezőjük és a quoteMapIds-ba tesszük.
        Set<Id> quoteIds = new Set<Id>();
        for(QuoteLineItem qli : triggerNew){
            if(qli.QuoteId != null && triggerOldMap.get(qli.Id).Discount != qli.Discount){
                quoteIds.add(qli.QuoteId);
            }
            
        }        
        Map<Id,Quote> quoteMapIds = new Map<Id,Quote>([SELECT Id, Status, (SELECT Id, Discount FROM QuoteLineItems) FROM Quote WHERE Id IN :quoteIds]); 
        
        //Összehasonlitjuk a QuoteLineItem régi Discount mezőjét az új Discount mezővel, a Quote Status mezőjét ennek megfelelően állitjuk.
        for(QuoteLineItem qli : triggerNew){
            if(qli.QuoteId != null && triggerOldMap.get(qli.Id).Discount != qli.Discount){
                if(quoteMapIds.get(qli.QuoteId).Status !='Accepted' || quoteMapIds.get(qli.QuoteId).Status !='Denied' || quoteMapIds.get(qli.QuoteId).Status !='Rejected' || quoteMapIds.get(qli.QuoteId).Status !='Presented'){
                    if(triggerOldMap.get(qli.Id).Discount == null && qli.Discount != null){//Ha Discount-ot adunk meg, akkor a Quote Status mezőjét In Review-re állitjuk.
                        quoteMapIds.get(qli.QuoteId).Status = INREVIEW;
                    }else if(triggerOldMap.get(qli.Id).Discount != null && qli.Discount == null){//Ha töröljük a Discount mezőt, akkor egy metódussal megnézzük, hogy van-e még Discount-os QuoteLineItem.
                        List<QuoteLineItem> qliList =  quoteMapIds.get(qli.QuoteId).QuoteLineItems;
                        quoteMapIds.get(qli.QuoteId).Status = quoteHasInReviewQuoteLineItem(qliList);
                    }
                }
            }            
        }  
       
		update quoteMapIds.values();
    }
    
    
    public static void handleAfterDelete(List<QuoteLineItem> triggerOld){
        
        //Kikeressük azokat a Quote-eket, amelyeknek közük van a törölt QuoteLineItem-ekhoz és a quoteMapIds-ba tesszük.
        Set<Id> quoteIds = new Set<Id>();
        for(QuoteLineItem qli : triggerOld){
            if(qli.QuoteId != null){
                quoteIds.add(qli.QuoteId);
            }            
        }        
        Map<Id,Quote> quoteMapIds = new Map<Id,Quote>([SELECT Id, Status, (SELECT Id, Discount FROM QuoteLineItems) FROM Quote WHERE Id IN :quoteIds]);
        
        //Megvizsgáljuk, hogy az adott Quote QuteLineItem lista méretét.
        for(Id qId : quoteMapIds.keySet()){
            if(quoteMapIds.get(qId).QuoteLineItems.size() == 0){
                quoteMapIds.get(qId).Status = DRAFT;//Ha nulla, azaz nincs több QuoteLineItem a Quote-on, akkor Quote Status mezőjét Draft-ra állitjuk.
            }      
            else if(quoteMapIds.get(qId).QuoteLineItems.size() != 0){
                
                	List<QuoteLineItem> qliList =  quoteMapIds.get(qId).QuoteLineItems;
                	quoteMapIds.get(qId).Status = quoteHasInReviewQuoteLineItem(qliList);//Ha nem nulla, akkor egy metódussal megnézzük, hogy van-e még Discount-os QuoteLineItem.
                
            }                       
        }
        update quoteMapIds.values();
    }
    
    private static String quoteHasInReviewQuoteLineItem(List<QuoteLineItem> qliList){
        Boolean quoteHasInReviewedQuoteLineItems = false;
        String statusOfQuote = '';
        for(QuoteLineItem qli : qliList){
            if(qli.Discount != null){
                quoteHasInReviewedQuoteLineItems = true;
            }                    
        }
        System.debug('QuoteLineItem lista mérete: '+qliList.size());
        System.debug('Van In Review qli rekord? '+quoteHasInReviewedQuoteLineItems);               
        if(quoteHasInReviewedQuoteLineItems == false){
            statusOfQuote = APPROVED;
        }else if(quoteHasInReviewedQuoteLineItems == true){
            statusOfQuote = INREVIEW;
        }
        return statusOfQuote;
    }
}
