trigger QuoteTrigger on Quote (before insert, before update, after insert, after update, after delete) {
    
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            QuoteTriggerHandler.handleBeforeInsert(Trigger.New);
        }else if(Trigger.isUpdate){
            QuoteTriggerHandler.handleBeforeUpdate(Trigger.oldMap, Trigger.New);
        }        
    }else if(Trigger.isAfter){
        if(Trigger.isInsert){
            QuoteTriggerHandler.handleAfterInsert(Trigger.New);
        }else if(Trigger.isUpdate){
            QuoteTriggerHandler.handleAfterUpdate(Trigger.oldMap, Trigger.New);
        }else if(Trigger.isDelete){
            QuoteTriggerHandler.handleAfterDelete(Trigger.old);
        }       
    }

}